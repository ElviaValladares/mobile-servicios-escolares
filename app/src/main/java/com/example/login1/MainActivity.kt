package com.example.login1

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageButton

class MainActivity : AppCompatActivity(), View.OnClickListener {
    private var imgButtonGo: ImageButton?=null

    override fun onClick(v: View?) {
        when(v!!.id){
            R.id.btLogin->{
                Log.i("onClick","txtNewAcc")
                val intent= Intent(this,AdminMenuActivity::class.java)
                startActivity(intent)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        this.initializeGUI()
    }

    private fun initializeGUI() {
        this.imgButtonGo=this.findViewById(R.id.btLogin)
        this.imgButtonGo!!.setOnClickListener(this)
    }

}
