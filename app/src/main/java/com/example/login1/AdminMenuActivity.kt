package com.example.login1

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageButton

class AdminMenuActivity : AppCompatActivity(), View.OnClickListener  {

    private var imgButtonGo: ImageButton?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_admin_menu)
        this.initializeGUI()
    }

    private fun initializeGUI() {
        this.imgButtonGo=this.findViewById(R.id.btGo)
        this.imgButtonGo!!.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v!!.id){
            R.id.btGo->{
                Log.i("onClick","txtNewAcc")
                val intent= Intent(this,NewUserActivity::class.java)
                startActivity(intent)
            }
        }
    }
}
